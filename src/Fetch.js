import './App.css';
import { Component } from 'react';

class App extends Component {
    state = {
        posts: []
    };

    // Lifecycles Methods Did Mount = Neste caso chama o método da classe 'handleTimeOut' quando o componete for montado.
    componentDidMount() {
        this.loadPosts();
    }

    loadPosts = async () => {
        const postResponse = fetch('https://jsonplaceholder.typicode.com/posts')
        const photosResponse = fetch('https://jsonplaceholder.typicode.com/photos')

        // Aguarda todas as buscas que foram feitas com fetch sejam concluídas. Os dados são armazenados nas variáveis 'post' e 'photos'.
        const [post, photos] = await Promise.all([postResponse, photosResponse])

        const postJson = await post.json()
        const photosJson = await photos.json()

        const postsAndphots = postJson.map((post, index) => {
            return { ...post, cover: photosJson[index].url }
        })

        this.setState({ posts: postsAndphots })
    }

    render() {
        const { posts } = this.state;

        return (
            <section className='container'>
                <div className="posts">
                    {posts.map(post => (
                        <div key={post.id} className='post'>
                            <img src={post.cover} alt='Imagem dos Post' />
                            <div className='post-content'>
                                <h1>{post.title}</h1>
                                <p>{post.body}</p>
                            </div>
                        </div>
                    ))}
                    olá
                </div>
            </section>
        );
    }
}

export default App;
