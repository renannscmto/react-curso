export const loadPosts = async () => {
    const postResponse = fetch('https://jsonplaceholder.typicode.com/posts')
    const photosResponse = fetch('https://jsonplaceholder.typicode.com/photos')

    // Aguarda todas as buscas que foram feitas com fetch sejam concluídas. Os dados são armazenados nas variáveis 'post' e 'photos'.
    const [post, photos] = await Promise.all([postResponse, photosResponse])

    const postJson = await post.json()
    const photosJson = await photos.json()

    const postsAndphots = postJson.map((post, index) => {
        return { ...post, cover: photosJson[index].url }
    })

    return postsAndphots
}