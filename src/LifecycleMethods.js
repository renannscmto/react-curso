import './App.css';
import { Component } from 'react';

class Lifecycles extends Component {
    state = {
        counter: 0,
        posts: [{
            id: 1,
            title: "Title 1",
            text: "Text 1"
        },
        {
            id: 2,
            title: "Title 2",
            text: "Text 2"
        },
        {
            id: 3,
            title: "Title 3",
            text: "Text 3"
        }]
    };

    uptadeTimeout = null

    // Lifecycles Methods Did Mount = Neste caso chama o método da classe 'handleTimeOut' quando o componete for montado.
    componentDidMount() {
        this.handleTimeOut();
    }

    // Lifecycles Methods Did Update = Neste caso chama o método da classe 'handleTimeOut' quando o componente for atualizado.
    componentDidUpdate() {
        this.handleTimeOut();
    }

    // Lifecycles Methods Will Unmount = Limpa o métodp 'updateTimeout' quando o componente for desmontado.
    componentWillUnmount() {
        clearTimeout(this.uptadeTimeout)
    }

    handleTimeOut = () => {
        const { counter, posts } = this.state;
        posts[0].title = "Title changed"

        // A cada segundo é atualizado o atributo 'counter' do state pois o método 'componentDidUpdate' gera um laço infinto, causando conflito com o método 'updatetimeout'.
        setTimeout(() => {
            this.uptadeTimeout = this.setState({ posts, counter: counter + 1 });
        }, 1000)
    }

    render() {
        const { posts, counter } = this.state;

        return (
            <div className="App" >
                {counter}
                {posts.map(post => (
                    <div key={post.id}>
                        <h1>{posts.title}</h1>
                        <p>{posts.text}</p>
                    </div>
                ))}
            </div>
        );
    }
}

export default Lifecycles;
