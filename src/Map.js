import './App.css';
import { Component } from 'react';

class Map extends Component {
    state = {
        posts: [
            {
                id: 1,
                title: "Title 1",
                text: "Text 1"
            },
            {
                id: 2,
                title: "Title 2",
                text: "Text 2"
            },
            {
                id: 3,
                title: "Title 3",
                text: "Text 3"
            }
        ]
    };

    render() {
        const { posts } = this.state;

        return (
            <div className="App" >
                {posts.map(post => (
                    <div key={post.id}>
                        <h1>{posts.title}</h1>
                        <p>{posts.text}</p>
                    </div>
                ))}
            </div>
        );
    }
}

export default Map;
