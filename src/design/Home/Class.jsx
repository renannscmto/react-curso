import './style.css';
import { Component } from 'react';

import { loadPosts } from '../../utils/load-posts';
import { Posts } from '../../components/Posts';
import { Button } from '../../components/Button';
import { Input } from '../../components/Input';

class App extends Component {
  state = {
    posts: [],
    allPosts: [],
    page: 0,
    postPerPage: 5,
    searchPosts: ''
  }

  // Lifecycles Methods Did Mount = Neste caso chama o método da classe 'handleTimeOut' quando o componete for montado.
  async componentDidMount() {
    await this.loadPosts()
  }

  loadPosts = async () => {
    const { page, postPerPage } = this.state

    const postsAndphots = await loadPosts()
    this.setState({
      posts: postsAndphots.slice(page, postPerPage), // Método 'slice()' realiza um fatiamento do array onde o primeiro argumento é o valor de ínicio e segundo argumento tem o valor final.
      allPosts: postsAndphots
    })
  }

  loadMorePosts = () => {
    const { posts, allPosts, page, postPerPage } = this.state

    const nextPage = page + postPerPage
    const nextPosts = allPosts.slice(nextPage, nextPage + postPerPage)
    posts.push(...nextPosts)

    this.setState({ ...posts, page: nextPage })
  }

  handleChange = (e) => {
    const { value } = e.target
    this.setState({ searchPosts: value })
  }

  render() {
    const { posts, page, postPerPage, allPosts, searchPosts } = this.state
    const noPosts = page + postPerPage > allPosts.length

    // Filtrando os posts á partir do texto renderizado no input, onde é feita um condição que tranforma o valor de 'serachPosts' em boolean para verificar se temos texto dígitado, retornando o titulo do post que está incluso na pesquisa.
    const filterPosts = !!searchPosts ?
      allPosts.filter(post => {
        return post.title.toLowerCase().includes(searchPosts.toLowerCase())
      }) : posts


    return (
      <section className='container'>
        {!!searchPosts && (<h1> Search Post: {searchPosts} </h1>)}

        <Input searchPosts={searchPosts} onChange={this.handleChange} />

        <div className="posts">
          <Posts posts={filterPosts} />
        </div>

        {!searchPosts && (
          <Button disabled={noPosts} text={'Load More Posts'} onClick={this.loadMorePosts} />
        )}
      </section>
    );
  }
}

export default App
