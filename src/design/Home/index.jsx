import { useCallback, useEffect, useState } from 'react';

import './style.css';

import { loadPosts } from '../../utils/load-posts';
import { Posts } from '../../components/Posts';
import { Button } from '../../components/Button';
import { Input }  from '../../components/Input';

export const Home = () => {
    const [posts, setPosts] = useState([])
    const [allPosts, setAllPosts] = useState([])
    const [page, setPage] = useState(0)
    const [postPerPage] = useState(5)
    const [searchPosts, setSearchPosts] = useState('')

    const handleLoadPosts = useCallback( async (page, postPerPage) => {
        const postsAndphots = await loadPosts()

        setPosts(postsAndphots.slice(page, postPerPage))
        setAllPosts(postsAndphots)
    }, [])

    useEffect(() => {
      handleLoadPosts(0, postPerPage)
    }, [handleLoadPosts, postPerPage])

    const loadMorePosts = () => {
      const nextPage = page + postPerPage
      const nextPosts = allPosts.slice(nextPage, nextPage + postPerPage)
      posts.push(...nextPosts)

      setPosts(posts)
      setPage(nextPage)
    }

    const handleChange = (e) => {
        const { value } = e.target
        setSearchPosts(value)
    }

    const noPosts = page + postPerPage >= allPosts.length

    const filterPosts = !!searchPosts ?
      allPosts.filter(post => {
        return post.title.toLowerCase().includes(searchPosts.toLowerCase())
    }) : posts;


    return (
      <section className='container'>
        {!!searchPosts && (<h1> Search Post: {searchPosts} </h1>)}

        <Input searchPosts={searchPosts} onChange={handleChange} />

        <Posts posts={filterPosts} />

        {!searchPosts && <Button disabled={noPosts} text={'Load More Posts'} onClick={loadMorePosts} />}
      </section>
    );
};


