import "./style.css"

export const PostCard = ({ id, cover, title, body }) => (
    <div key={id} className='post'>
        <img src={cover} alt='Imagem dos Post' />
        <div className='post-content'>
            <h1>{title}</h1>
            <p>{body}</p>
        </div>
    </div>
)
