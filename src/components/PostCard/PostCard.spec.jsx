import { render, screen } from "@testing-library/react"
import { PostCard } from "."
import {propsMock} from './mock'

const props = propsMock

describe('<PostCard />', () => {
    it('should render correty', () => {
        render(<PostCard {...props} />)

        expect(screen.getByRole('img', {name: 'Imagem dos Post'})).toHaveAttribute('src', 'img/img.png')
        expect(screen.getByRole('heading', {name: 'Title 1'})).toBeInTheDocument()
        expect(screen.getByText('Content 1')).toBeInTheDocument()
    })

    it('should macth snapshot', () => {
        const {container} = render(<PostCard {...props} />)
        expect(container.firstChild).toMatchSnapshot()  
    })
})