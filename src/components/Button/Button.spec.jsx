import { render, screen } from "@testing-library/react";
import { Button } from ".";
import userEvent from "@testing-library/user-event";

// Descrevendo um grupo de teste para o componente <Button/>
describe('<Button/>', () => {
    it('should render Button in screen with text: "Load more"', () => {
        // Rendezirando o componente com o atributo que deve conter.
        render(<Button text="Load more" />);
        // Número de asserções que o teste deve ter.
        expect.assertions(1)
        // Verificando se a tela possui a regra/elemento 'button' e se este elemento possui o texto indicado.
        const button = screen.getByRole('button', { name: /Load more/i });
        // Verifica se o botão está np documento/dom.
        expect(button).toBeInTheDocument();
    });

    it('should call function on Button on click', () => {
        // Usando a função mock para imitar a chamada de uma função.
        const fn = jest.fn()
        // Rendezirando o componente com o atributo que deve conter.
        render(<Button text="Load more" onClick={fn} />)
        // Verificando se a tela possui a regra/elemento 'button' e se este elemento possui o texto indicado.
        const button = screen.getByRole('button', { name: /Load more/i })
        // Disparando a função com 'userEvent' no click do botão.
        userEvent.click(button)
        // Verifica se a função foi chamada apenas uma única vez.
        expect(fn).toHaveBeenCalledTimes(1);
    });

    it('should be disabled when disabled is: true', () => {
        // Rendezirando o componente com o atributo que deve conter.
        render(<Button text="Load more" disabled={true} />);
        // Verificando se a tela possui a regra/elemento 'button' e se este elemento possui o texto indicado.
        const button = screen.getByRole('button', { name: /Load more/i });
        // Verifica se elemento 'button' possui o atributo 'disabled' e se este atributo está desabilitado.
        expect(button).toBeDisabled();
    });

    it('should render snapshot', () => {
        const fn = jest.fn()
        const {container} = render(<Button text="Load more" disabled={true} onClick={fn}/>);
        expect(container).toMatchSnapshot();

    });

})
