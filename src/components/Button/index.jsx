import './style.css'

export const Button = ({ text, onClick, disabled = false }) => {
    return <button className="box-button" disabled={disabled} onClick={onClick}> {text} </button>
}
