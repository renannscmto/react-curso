import { fireEvent, render, screen } from "@testing-library/react"
import { Input } from "."
import userEvent from "@testing-library/user-event"

describe('<Input/>', () => {
    it('should render get value input', () => {
        render(<Input />)
        const input = screen.getByRole('textbox')

        userEvent.type(input, 'Testing input value')
        expect(input).toHaveValue('Testing input value')
    })

    it('should render exchange of value in input', () => {
        render(<Input />)
        const onchange = screen.getByRole('textbox')

        fireEvent.change(onchange, { target: {value: 'Teste onChange input'} })
        expect(onchange).toHaveValue('Teste onChange input')
    })

    it('should render snapshot <Input />', () => {
        const { container } = render(<Input />)
        expect(container).toMatchSnapshot()

    })
})
